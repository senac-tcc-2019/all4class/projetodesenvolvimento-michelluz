<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequisicaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requisicao', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomeCriador', 40);
            $table->string('emailCriador', 40);
            $table->string('telefoneCriador', 40);
            $table->string('descricao', 1000);
            $table->integer('escola_id')->unsigned();
            $table->string('vulnerabilidade_id', 100);
            $table->integer('instituicao_id')->unsigned();
            $table->boolean('isLida')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requisicao');
    }
}
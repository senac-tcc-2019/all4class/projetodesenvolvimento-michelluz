<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>All4Class</title>

    <!-- Bootstrap core CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/business-frontpage.css" rel="stylesheet">

  </head>

  <body id="mapa">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto" id="menuSuperior">
            <li class="nav-item" id="paginaInicial">
              <a class="nav-link" href="/">Inicial
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item active" id="paginaMapa">
              <a class="nav-link" href="{{ route('mapa') }}">Mapa</a>
            </li>
            <li class="nav-item" id="paginaParticipe">
              <a class="nav-link" href="{{ route('participe') }}">Participe</a>
            </li>
            <li class="nav-item" id="paginaLogin">
              <a class="nav-link" href="{{ route('login') }}">Login</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
      <div id="googleMap" style="width:100%; height:600px"></div>
      <div class="container">
        <br>
        <div class="row">
          <div class="col-sm-12">
            
          </div>
        </div>
        <hr>
        <br>
          <div class="com-sm-12">
            <h4>Relação de requisições já efetuadas</h4>
            <table class="table table-hover">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Órgão</th>
                  <th scope="col">Escola</th>
                  <th scope="col">Bairro</th>
                  <th scope="col">Data</th>
                </tr>
              </thead>
              <tbody>
              <?php
                use App\Requisicao;
                $requisicao = Requisicao::paginate(10);
              ?>
                @foreach($requisicao as $p)
                <tr>
                  <th scope="row">{{$p->id}}</th>
                  <td>{{$p->instituicao->nome}}</td>
                  <td>{{$p->escola->nome}}</td>
                  <td>{{$p->escola->bairro->nome}}</td>
                  <td>{{$p->created_at}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
            {{$requisicao->links()}}
          <div class="col-sm-12" style="text-align:center">
            <a href="{{route('relatorio')}}" class="btn btn-info">Confira o as estatísticas de requisições!</a>
          </div>
        </div>
      </div>
      <div class="row"><br><br></div>
      <script>
      function myMap() {
        var geocoder = new google.maps.Geocoder;
      var mapProp= {
          center:new google.maps.LatLng(-31.7290769,-52.3556214),
          zoom:12,
      };
      
      @foreach($requisicao as $p)

        geocoder.geocode({'address': "{{$p->escola->nome}}, pelotas"}, function(results, status) {
          if (status === 'OK') {
            map.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
              map: map,
              position: results[0].geometry.location
            });
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });

      @endforeach

      var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
      };
      </script>
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDuK6yOn7FolY_TmaId5e7vx5VAIVLdlzM&callback=myMap"></script>
    <!-- Header with Background Image -->
    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Class4All 2018</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>

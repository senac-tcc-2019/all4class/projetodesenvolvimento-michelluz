@extends('adminlte::page')

@section('title', 'Edição de Instituição')

@section('content')

    <div class='col-sm-11'>
        
        <h2> Edição de Instituição </h2>
    
    </div>
    <div class='col-sm-1'>
        <a href="{{ route('instituicao.index') }}" class="btn btn-primary"
           role="button">Voltar</a>
    </div>

    

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        
        <form method="POST" action="{{ route('instituicao.update', $instituicao->id) }}"
 
                enctype="multipart/form-data">
                {{method_field('PUT')}}
               {{ csrf_field() }}
        <div class="row">
            <div class="col-sm-6">        
                <div class="form-group">
                    <label for="nome">Nome da Instituição:</label>
                    <input type="text" id="nome" name="nome" required class="form-control" value="{{$instituicao->nome}}">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="endereco">Endereço</label>
                    <input type="text" id="endereco" name="endereco" required class="form-control" value="{{$instituicao->endereco}}">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="responsavel">Responsável</label>
                    <select id="responsavel" name="responsavel_id" class="form-control">
                      @foreach($responsavel as $b)
                        <option value="{{$b->id}}" 
                                {{ ($instituicao->responsavel_id == $b->id) ? "selected" : "" }}>
                                {{$b->nome}}</option>
                      @endforeach
                    </select>  
                </div>
            </div>
        </div>

                        <button type="submit" class="btn btn-primary">Enviar</button>
                        <button type="reset" class="btn btn-warning">Limpar</button>
            </form>
                    
        


@endsection
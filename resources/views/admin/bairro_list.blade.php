@extends('adminlte::page')

@section('title', 'Cadastro de Bairros')

@section('content')
<div class='col-sm-12'>
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        @if (session('alert'))
            <div class="alert alert-danger">
                {{ session('alert') }}
            </div>
        @endif
    </div>

    <div class='col-sm-11'>
        <h2 id="BairroList"> Cadastro de Bairros </h2>
    </div>

            <div class="col-sm-1">
                <br>
            <a href="{{route('bairro.create')}}" class="btn btn-info"
               role="button">Novo</a>
            </div>
    <div class='col-sm-12'>



        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th align="right">Ação</th>
            </tr>
            </thead>
            <tbody>
            @foreach($bairro as $b)
                <tr>
                    <td style="text-align: left">{{$b->id}}</td>
                    <td>{{$b->nome}}</td>
                    
                    <td>
                        <form style="display: inline-block"
                            action="{{route('bairro.edit', $b->id)}}">
                              
                                  <button type="submit" title="Editar"
                                          class="btn btn-warning btn-sm"><i class="glyphicon glyphicon-pencil"></i></button>
                          
                        </form>  &nbsp;&nbsp;

                        <form style="display: inline-block"
                              method="post"
                              action="{{route('bairro.destroy', $b->id)}}"
                              onsubmit="return confirm('Confirma Exclusão?')">
                            {{method_field('delete')}}
                            {{csrf_field()}}
                            <button type="submit" title="Excluir"
                      class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i></button>
                        </form> &nbsp;&nbsp;

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
         <div class="col-sm-12"> {{ $bairro->links() }}</div>
    </div>

@endsection

@section('js')
  <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
@endsection

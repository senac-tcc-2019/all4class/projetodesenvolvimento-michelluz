@extends('adminlte::page')

@section('title', 'Resposta')

@section('content_header')
    <h1>Dados da Requisição <b> {{ $requisicao->id }}</b></h1>
@endsection

@section('content')
<div class="container">
  <div class="row">
    <div class="col-sm-4">
      <h3>Dados da Requisição</h3>
      <b>Autor: </b> {{ $requisicao->nomeCriador }}<br>
      <b>E-mail: </b> {{ $requisicao->emailCriador }}<br>
      <b>Telefone: </b> {{ $requisicao->telefoneCriador }} <br>
      <b>Descrição: </b> {{ $requisicao->descricao }} <br>
      <br>
      
    </div>
    <div class="col-sm-6">
      <h3>Responder</h3>
      <form action="{{route('resposta')}}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{ $requisicao->id }}" s>
        <div class="form-group">
          <label for="nome">Nome do Autor:</label>
          <input id="nome" name="nome" type="text" class="form-control" value="{{ $requisicao->nomeCriador }}" readonly>
        </div>
        <div class="form-group">
          <label for="email">E-mail:</label>
          <input id="email" name="email" type="text" class="form-control" value="{{ $requisicao->emailCriador }}" readonly>
        </div>
        <div class="form-group">
          <label for="email">Telefone:</label>
          <input id="telefone" name="telefone" type="text" class="form-control" value="{{ $requisicao->telefoneCriador }}" readonly>
        </div>
        <div class="form-group">
          <label for="mensagem">Resposta:</label>
          <textarea rows="4" cols="50" id="mensagem" name="mensagem" class="form-control"></textarea>
        </div>
        <input type="submit" class="btn btn-success" value="enviar">
      </form>
    </div>
  </div>
</div>

@endsection
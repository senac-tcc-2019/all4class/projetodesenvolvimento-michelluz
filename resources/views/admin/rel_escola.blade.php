<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <title>PDF File</title>
</head>
<body>
    
        <h2>Relatório de Escolas Cadastradas</h2>

        <table border=1>
                <tr>
                    <th> Nome </th>
                    <th> Bairro </th>
                    <th> Endereço </th>
                    <th> Contato </th>

                </tr> 
                @foreach($escola as $e)
                <tr>
                  <td> {{$e->nome}} </td>
                  <td> {{$e->bairro->nome}} </td>
                  <td> {{$e->endereco}} </td>
                  <td> {{$e->responsavel->email}}</td>
                
                </tr>
                @endforeach
        
        </table>

</body>
</html>
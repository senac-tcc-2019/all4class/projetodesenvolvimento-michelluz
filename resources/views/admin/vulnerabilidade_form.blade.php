@extends('adminlte::page')

@section('title', 'Cadastro de Vulnerabilidade')

@section('content')

    <div class='col-sm-11'>
        
        <h2> Inclusão de Vulnerabilidade </h2>
    
    </div>
    <div class='col-sm-1'>
        <a href="{{route('vulnerabilidade.index')}}" class="btn btn-primary"
           role="button">Voltar</a>
    </div>

    

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        
            <form method="post" action="{{route('vulnerabilidade.store')}}">
                
                {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="titulo">Título:</label>
                        <input type="text" class="form-control" id="titulo"
                                name="titulo"
                            
                                required>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="descricao">Descrição:</label>
                        <input type="text" class="form-control" id="descricao"
                                name="descricao" required>
                    </div>
                </div>      
            </div>     
    
    <button type="submit" class="btn btn-primary">Enviar</button>
    <button type="reset" class="btn btn-warning">Limpar</button>
                    </form>


@endsection
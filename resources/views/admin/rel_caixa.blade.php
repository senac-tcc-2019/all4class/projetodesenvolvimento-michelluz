<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <title>PDF File</title>
</head>
<body>
    
    <div class='col-sm-12'>
        <h2> Export de Requisições </h2>

        
        @forelse($requisicao as $r)
        <table border=1 width="400">
            <tr>
                <th>Código</th>
                <td>{{$r->id}}</td>
            </tr>
            <tr>
                <th>Autor da Requisição</th>
                <td>{{$r->nomeCriador}}</td>
            </tr>
            <tr>
                <th>Email</th>
                <td>{{$r->emailCriador}}</td>
            </tr>
            <tr>
                <th>Telefone</th>
                <td>{{$r->telefoneCriador}}</td>
            </tr>
            <tr>
                <th>Descrição</th>
                <td>{{$r->descricao}}</td>
            </tr>
            <tr>
                <th>Escola</th>
                <td>{{$r->escola->nome}}</td>
            </tr>
            <tr>
                <th>Perfil dos Alunos</th>
                <td>{{$r->vulnerabilidade->titulo}}</td>
            </tr>
            <tr>
                <th>Corporação acionada</th>
                <td>{{$r->instituicao->nome}}</td>
                
            </tr>
        </table>
        <br>
        <br>
        <br>
        
                
            @empty

                <h4>Não existem proposta cadastradas ainda.</h4>
            
            
            @endforelse
        
    </div>


</body>
</html>
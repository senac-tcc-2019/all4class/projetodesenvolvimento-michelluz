@extends('adminlte::page')

@section('title', 'Cadastro de Vulnerabilidade')

@section('content')
<div class='col-sm-12'>
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
    </div>

    <div class='col-sm-11'>
        <h2 id="VulnerabilidadeList"> Cadastro de Vulnerabilidade </h2>
    </div>

            <div class="col-sm-1">
                <br>
            <a href="{{route('vulnerabilidade.create')}}" class="btn btn-info"
               role="button">Novo</a>
            </div>
    <div class='col-sm-12'>



        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Título</th>
                <th>Descrição</th>
            </tr>
            </thead>
            <tbody>
            @foreach($vulnerabilidade as $b)
                <tr>
                    <td style="text-align: left">{{$b->id}}</td>
                    <td>{{$b->titulo}}</td>
                    <td>
                        <form style="display: inline-block"
                              method="post"
                              action="{{route('vulnerabilidade.destroy', $b->id)}}"
                              onsubmit="return confirm('Confirma Exclusão?')">
                            {{method_field('delete')}}
                            {{csrf_field()}}
                            <button type="submit" title="Excluir"
                      class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i></button>
                        </form> &nbsp;&nbsp;

                    </td>
                </tr>
    
            @endforeach
            </tbody>
        </table>
         <div class="col-sm-12"> {{ $vulnerabilidade->links() }}</div>
    </div>

@endsection

@section('js')
  <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
@endsection

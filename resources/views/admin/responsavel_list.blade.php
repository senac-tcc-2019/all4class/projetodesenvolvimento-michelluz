@extends('adminlte::page')

@section('title', 'Cadastro de Responsável')

@section('content_header')
    <h1 id="ResponsavelList">Cadastro de Responsável
    <a href="{{ route('responsavel.create') }}" 
       class="btn btn-primary pull-right" role="button">Novo</a>
    </h1>
@endsection

@section('content')

<div class='col-sm-12'>
  @if (session('status'))
      <div class="alert alert-success">
          {{ session('status') }}
      </div>
  @endif

  @if (session('alert'))
      <div class="alert alert-danger">
          {{ session('alert') }}
      </div>
  @endif
</div>

<form method="POST" action="{{ route('responsavel.pesquisa') }}"  class="form form-inline">
  {{ csrf_field() }}
  <input type="text" name="nome" class="form-control" placeholder="NOME">
  <input type="text" name="telefone" class="form-control" placeholder="TELEFONE">
  <input type="text" name="email" class="form-control" placeholder="EMAIL">
  
  
  <button type="submit" class="btn btn-primary">Pesquisar</button>
</form>

<table class="table table-striped">
  <tr>
    <th> Nome </th>
    <th> E-mail </th>
    
    <th> Telefone </th>
    <th> Ações </th>
  </tr>  
@forelse($responsavel as $e)
  <tr>
    <td> {{$e->nome}} </td>
    <td> {{$e->email}} </td>
    
    <td> {{$e->telefone}} </td>
  
    <td> 
        <form style="display: inline-block"
        action="{{route('responsavel.edit', $e->id)}}">
          
              <button type="submit" title="Editar"
                      class="btn btn-warning btn-sm"><i class="glyphicon glyphicon-pencil"></i></button>
      
        </form>  &nbsp;&nbsp;
        
        <form style="display: inline-block"
              method="post"
              action="{{route('responsavel.destroy', $e->id)}}"
              onsubmit="return confirm('Confirma Exclusão?')">
               {{method_field('delete')}}
               {{csrf_field()}}
              <button type="submit" title="Excluir"
                      class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i></button>
        </form>  &nbsp;&nbsp;
        
    </td>
  </tr>

@empty
  <tr><td colspan=8> Não há Responsáveis cadastrados ou filtro da pesquisa não 
                     encontrou registros </td></tr>
@endforelse
</table>
<div class="col-sm-12"> {{ $responsavel->links() }}</div>
@endsection

@section('js')
  <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
@endsection
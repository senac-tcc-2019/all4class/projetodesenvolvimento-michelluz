
@extends('adminlte::page')

@section('title', 'Lista de Requisições')

@section('content')

    <div class='col-sm-12'>
        <h1 id="RequisicaoList"> Requisições 
            <a href="{{ route('requisicao.export') }}" 
               class="btn btn-primary pull-right" role="button">Export</a>
               
        </h1>
        
    </div>
    @if (Session::has('success'))
	<div class="alert alert-success" role="alert">
		<strong>Sucesso:</strong> {{ Session::get('success') }}
	</div>
    @elseif (Session::has('error'))
	<div class="alert alert-danger" role="alert">
		<strong>Erro:</strong> {{ Session::get('error') }}
	</div>
    @endif

@if (count($errors) > 0)
	<div class="alert alert-danger" role="alert">
		<strong>Erros:</strong> 
		<ul>
		@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
		</ul>
	</div
@endif
<div class='col-sm-12'>       
    <form method="POST" action="{{ route('requisicao.pesquisa') }}"  class="form form-inline">
        {{ csrf_field() }}
        <input type="text" name="escola" class="form-control" placeholder="ESCOLA">
        <label for="vulnerabilidade">Vulnerabilidade</label>
        <select id="vulnerabilidade" name="vulnerabilidade" class="form-control" placeholder="VULNERABILIDADE">
            <option selected="selected"></option>
          @foreach($vulnerabilidade as $v)
            <option value="{{$v->id}}" 
                    {{ ((isset($reg) and $reg->vulnerabilidade == $v->id) or 
                       old('vulnerabilidade_id') == $v->id) ? "selected" : "" }}>
                    {{$v->titulo}}</option>
          @endforeach
        </select>  
        <input type="text" name="email" class="form-control" placeholder="EMAIL">
        <input type="date" name="data" class="form-control" placeholder="DATA">
        <button type="submit" class="btn btn-primary">Pesquisar</button>
    </form>
</div>
    <div class='col-sm-12'>
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <table class="table table-striped">
            <tr>
                <th>Código</th>
                <th>Autor da Requisição</th>
                <th>Email</th>
                <th>Telefone</th>
                <th>Descrição</th>
                <th>Escola</th>
                <th>Perfil dos Alunos</th>
                <th>Corporação acionada</th>
                <th>Responder</th>
                
            </tr>
            @forelse($requisicao as $r)
                <tr>
                    <td style="text-align: center">{{$r->id}}</td>
                    <td>{{$r->nomeCriador}}</td>
                    <td>{{$r->emailCriador}}</td>
                    <td>{{$r->telefoneCriador}}</td>
                    <td>{{$r->descricao}}</td>
                    <td>{{$r->escola->nome}}</td>
                    <td>{{$r->vulnerabilidade->titulo}}</td>
                    <td>{{$r->instituicao->nome}}</td>
                    <td> <a href="{{route('requisicao.resposta', $r->id)}}"
                        class="btn btn-info"
                        role="button">Responder</a> &nbsp;&nbsp;</td>
                </tr>
            @empty

                <h4>Não existem proposta cadastradas ainda.</h4>

            @endforelse
        </table>
        <div class="col-sm-12"> {{ $requisicao->links() }}</div>
    </div>

@endsection
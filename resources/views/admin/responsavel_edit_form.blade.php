@extends('adminlte::page')

@section('title', 'Cadastro de Responsável')

@section('content_header')


<h2>Inclusão de Responsável         

  <a href="{{ route('responsavel.index') }}" class="btn btn-primary pull-right" role="button">Voltar</a>
</h2>

@endsection

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<form method="POST" action="{{ route('responsavel.update', $responsavel->id) }}"
 
        enctype="multipart/form-data">
        {{method_field('PUT')}}
       {{ csrf_field() }}

<div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        <label for="nome">Nome:</label>
        <input type="text" id="nome" name="nome" value="{{$responsavel->nome}}" required 
               
               class="form-control">
      </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
          <label for="email">E-mail:</label>
          <input type="text" id="email" name="email" value="{{$responsavel->email}}" required 
                 
                 class="form-control">
        </div>
    </div>
  
    

    <div class="col-sm-6">
        <div class="form-group">
          <label for="nome">Endereço:</label>
          <input type="text" id="endereco" name="endereco" value="{{$responsavel->endereco}}" required 
                 
                 class="form-control">
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
          <label for="nome">Telefone:</label>
          <input type="text" id="telefone" name="telefone" value="{{$responsavel->telefone}}" required 
                 
                 class="form-control">
        </div>
    </div>
  </div>              


  <input type="submit" value="Enviar" class="btn btn-success">
  <input type="reset" value="Limpar" class="btn btn-warning">
</form>

@endsection

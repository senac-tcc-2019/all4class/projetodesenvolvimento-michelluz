@extends('adminlte::page')

@section('title', 'Cadastro de Escolas')

@section('content_header')
    <h1 id="EscolaList">Cadastro de Escolas
    <a href="{{ route('escola.create') }}" 
       class="btn btn-primary pull-right" role="button">Novo</a>
       
        <a href="{{ route('escola.export') }}" 
           class="btn btn-primary pull-right" role="button">Export</a>
    </h1>
    
@endsection

@section('content')

@if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>  
@endif

@if (session('alert'))
<div class="alert alert-danger">
      {{ session('alert') }}
    </div>  
@endif


<form method="POST" action="{{ route('escola.pesquisa') }}"  class="form form-inline">
    {{ csrf_field() }}
    <input type="text" name="nome" class="form-control" placeholder="NOME">
    <label for="bairro">Bairro</label>
        <select id="bairro" name="bairro" class="form-control" placeholder="BAIRRO">
          <option></option>
          @foreach($bairro as $b)
            <option value="{{$b->id}}" 
                    {{ ((isset($reg) and $reg->bairro == $b->id) or 
                       old('bairro_id') == $b->id) ? "selected" : "" }}>
                    {{$b->nome}}</option>
          @endforeach
        </select>  
    <input type="text" name="responsavel" class="form-control" placeholder="CONTATO">
    <button type="submit" class="btn btn-primary">Pesquisar</button>
</form>
<div class="table-responsive">
  <table class="table v-middle text-nowrap">
      <thead>
        <tr class="bg-light">
          <th class="border-top-0"> Nome </th>
          <th class="border-top-0"> Bairro </th>
          <th class="border-top-0"> Endereço </th>
          <th class="border-top-0"> Contato </th>
          <th class="border-top-0"> Ações </th>
        </tr>  
      <thead>
@forelse($escola as $e)
  <tr>
    <td> {{$e->nome}} </td>
    <td> {{$e->bairro->nome}} </td>
    <td> {{$e->endereco}} </td>
    <td> {{$e->responsavel->email}}</td>
  
    <td> 
        
      <form style="display: inline-block"
        action="{{route('escola.edit', $e->id)}}">
          
              <button type="submit" title="Editar"
                      class="btn btn-warning btn-sm"><i class="glyphicon glyphicon-pencil"></i></button>
      
      </form>  &nbsp;&nbsp; 

      <form style="display: inline-block"
        method="post"
        action="{{route('escola.destroy', $e->id)}}"
        onsubmit="return confirm('Confirma Exclusão?')">
          {{method_field('delete')}}
          {{csrf_field()}}
              <button type="submit" title="Excluir"
                      class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i></button>
      </form>  &nbsp;&nbsp;
    </td>
  </tr>


@empty
  <tr><td colspan=8> Não há Escolas cadastradas ou filtro da pesquisa não 
                     encontrou registros </td></tr>
@endforelse
</table>
</div>
<div class="col-sm-12"> {{ $escola->links() }}</div>
@endsection

@section('js')
  <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
@endsection
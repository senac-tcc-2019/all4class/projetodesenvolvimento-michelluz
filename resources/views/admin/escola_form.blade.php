@extends('adminlte::page')

@section('title', 'Cadastro de Carros')

@section('content_header')


<h2 id="EscolaForm">Inclusão de Escolas         

  <a href="{{ route('escola.index') }}" class="btn btn-primary pull-right" role="button">Voltar</a>
</h2>

@endsection

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<form method="POST" action="{{ route('escola.store') }}"
 enctype="multipart/form-data">
        
{{ csrf_field() }}

<div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        <label for="nome">Nome da escola</label>
        <input type="text" id="nome" name="nome" required 
               
               class="form-control">
      </div>
      <div class="form-group">
        <label for="responsavel">Responsável</label>
        <select id="responsavel" name="responsavel_id" class="form-control">
          @foreach($responsavel as $b)
            <option value="{{$b->id}}" 
                    {{ ((isset($reg) and $reg->responsavel == $b->id) or 
                       old('responsavel_id') == $b->id) ? "selected" : "" }}>
                    {{$b->nome}}</option>
          @endforeach
        </select>  
      </div>
    </div>

    <div class="col-sm-6">
      <div class="form-group">
        <label for="bairro">Bairro</label>
        <select id="bairro" name="bairro_id" class="form-control">
          @foreach($bairro as $b)
            <option value="{{$b->id}}" 
                    {{ ((isset($reg) and $reg->bairro == $b->id) or 
                       old('bairro_id') == $b->id) ? "selected" : "" }}>
                    {{$b->nome}}</option>
          @endforeach
        </select>  
      </div>

      <div class="form-group">
        <label for="endereco">Endereço</label>
        <input type="text" id="endereco" name="endereco" required 
               
               class="form-control">
      </div>
      
    </div>
    
  </div>              


  <input type="submit" value="Enviar" class="btn btn-success">
  <input type="reset" value="Limpar" class="btn btn-warning">
</form>

@endsection

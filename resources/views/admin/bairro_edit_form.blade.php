@extends('adminlte::page')

@section('title', 'Edição de Bairros')

@section('content')

    <div class='col-sm-11'>
        
        <h2 id="BairroForm" Edição de Bairro </h2>
    
    </div>
    <div class='col-sm-1'>
        <a href="{{route('bairro.index')}}" class="btn btn-primary"
           role="button">Voltar</a>
    </div>

    <div class='col-sm-12'>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        
        <form method="POST" action="{{ route('bairro.update', $bairro->id) }}"
 
                enctype="multipart/form-data">
                {{method_field('PUT')}}
               {{ csrf_field() }}

                <div class="form-group">
                    <label for="nome">Nome do Bairro:</label>
                    <input type="text" class="form-control" id="nome"
                            name="nome" value="{{$bairro->nome}}"
                            
                            required>
                </div>


                        <button type="submit" class="btn btn-primary">Enviar</button>
                        <button type="reset" class="btn btn-warning">Limpar</button>
                    </form>
    </div>


@endsection
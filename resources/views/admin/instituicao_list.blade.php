@extends('adminlte::page')

@section('title', 'Cadastro de Instituições')

@section('content_header')
    <h1 id="InstituicaoList">Lista de Instituições 
        <a href="{{ route('instituicao.create') }}" 
    class="btn btn-primary pull-right" role="button">Novo</a>
    </h1>
@endsection

@section('content')
<div class='col-sm-12'>
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    @if (session('alert'))
        <div class="alert alert-danger">
            {{ session('alert') }}
        </div>
    @endif
</div>

<form method="POST" action="{{ route('instituicao.pesquisa') }}"  class="form form-inline">
    {{ csrf_field() }}
    <input type="text" name="nome" class="form-control" placeholder="NOME">
    
    <button type="submit" class="btn btn-primary">Pesquisar</button>
</form>
    <table class="table table-striped">
        <tr>
            <th>ID</th>
            <th>Nome</th>
            <th>Responsável</th>
            <th>Email</th>
            <th>Telefone</th>
            <th>Ação</th>
        </tr>
        @foreach($instituicao as $b)
            <tr>
                <td style="text-align: left">{{$b->id}}</td>
                <td>{{$b->nome}}</td>
                <td>{{$b->responsavel->nome}}</td>}
                <td>{{$b->responsavel->email}}</td>}
                <td>{{$b->responsavel->telefone}}</td>}
                <td>

                    <form style="display: inline-block"
                        action="{{route('instituicao.edit', $b->id)}}">
                          
                              <button type="submit" title="Editar"
                                      class="btn btn-warning btn-sm"><i class="glyphicon glyphicon-pencil"></i></button>
                      
                    </form>  &nbsp;&nbsp;
                    <form style="display: inline-block"
                            method="post"
                            action="{{route('instituicao.destroy', $b->id)}}"
                            onsubmit="return confirm('Confirma Exclusão?')">
                        {{method_field('delete')}}
                        {{csrf_field()}}
                        <button type="submit" title="Excluir"
                    class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i></button>
                    </form> &nbsp;&nbsp;

                </td>
            </tr>
        @endforeach  
    </table>
@endsection

@section('js')
  <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
@endsection

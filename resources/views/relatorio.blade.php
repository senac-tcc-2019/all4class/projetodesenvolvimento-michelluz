<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>All4Class</title>

    <!-- Bootstrap core CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/business-frontpage.css" rel="stylesheet">

  </head>

  <body id="relatorio">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto" id="menuSuperior">
            <li class="nav-item" id="paginaInicial">
              <a class="nav-link" href="/">Inicial
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item" id="paginaMapa">
              <a class="nav-link" href="{{ route('mapa') }}">Mapa</a>
            </li>
            <li class="nav-item active" id="paginaParticipe">
              <a class="nav-link" href="{{ route('participe') }}">Participe</a>
            </li>
            <li class="nav-item" id="paginaLogin">
              <a class="nav-link" href="{{ route('login') }}">Login</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>        
  <div class="container">
      <h2>Relatórios visuais</i></h2>
        <br>
        <h4>Requisições por escola</h4>
        <?php
         use Illuminate\Support\Facades\DB;
         $dados = DB::select('select escola.nome as nome, count(*) as req from requisicao inner join escola on escola.id = requisicao.escola_id group by escola.id;');
        ?>
         <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
          google.charts.load("current", {packages:['corechart']});
          google.charts.setOnLoadCallback(drawChart);
          function drawChart() {
            var data = google.visualization.arrayToDataTable([
              ["Escola", "Requisições"],
              @foreach($dados as $d)
                ["{{$d->nome}}", {{$d->req}}],
              @endforeach
            ]);
            var view = new google.visualization.DataView(data);
            var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values_escola_registros"));
            chart.draw(view);
        }
        </script>
        <div id="columnchart_values_escola_registros" style="width: 900px; height: 300px;"></div>
        <hr>
        <h4>Requisições por bairro</h4>
        <?php
         $dados = DB::select('select bairro.nome as nome, count(*) ' . 
                             ' as req from requisicao ' . 
                             'inner join escola on escola.id = requisicao.escola_id ' . 
                             'inner join bairro on escola.bairro_id = bairro.id ' .
                             'group by bairro.id;');
        ?>
        <script type="text/javascript">
          google.charts.load("current", {packages:['corechart']});
          google.charts.setOnLoadCallback(drawChart);
          function drawChart() {
            var data = google.visualization.arrayToDataTable([
              ["Escola", "Requisições"],
              @foreach($dados as $d)
                ["{{$d->nome}}", {{$d->req}}],
              @endforeach
            ]);
            var view = new google.visualization.DataView(data);
            var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values_bairro_registros"));
            chart.draw(view);
        }
        </script>
        <div id="columnchart_values_bairro_registros" style="width: 900px; height: 300px;"></div>
        <hr>        
        <h4>Instituições requisitadas por bairro</h4>
        <?php
         $dados = DB::select('select instituicao.nome as nome, count(*) ' . 
                             ' as req from requisicao ' .  
                             'inner join instituicao on requisicao.instituicao_id = instituicao.id ' .
                             'group by instituicao.id;');
        ?>
         <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
          google.charts.load("current", {packages:['corechart']});
          google.charts.setOnLoadCallback(drawChart);
          function drawChart() {
            var data = google.visualization.arrayToDataTable([
              ["Escola", "Requisições"],
              @foreach($dados as $d)
                ["{{$d->nome}}", {{$d->req}}],
              @endforeach
            ]);
            var view = new google.visualization.DataView(data);
            var chart = new google.visualization.PieChart(document.getElementById("columnchart_values_instituicoes_registros"));
            chart.draw(view);
        }
        </script>
        <div id="columnchart_values_instituicoes_registros" style="width: 900px; height: 300px;"></div>
        <hr>
        <h4>Perfis de vulnerabilidade</h4>
        <?php
         $dados = DB::select('select requisicao.vulnerabilidade_id as nome, count(*) as req ' . 
                             'from requisicao ' .
                             'group by requisicao.vulnerabilidade_id;');
        ?>
         <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
          google.charts.load("current", {packages:['corechart']});
          google.charts.setOnLoadCallback(drawChart);
          function drawChart() {
            var data = google.visualization.arrayToDataTable([
              ["Escola", "Requisições"],
              @foreach($dados as $d)
                ["{{$d->nome}}", {{$d->req}}],
              @endforeach
            ]);
            var view = new google.visualization.DataView(data);
            var chart = new google.visualization.PieChart(document.getElementById("columnchart_values_perfis_registros"));
            chart.draw(view);
        }
        </script>
        <div id="columnchart_values_perfis_registros" style="width: 900px; height: 300px;"></div>

    </div>
     <!-- Header with Background Image -->
    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Class4All 2018</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>

# All4Class - Plataforma testada para comunicação entre comunidade escolar e órgãos públicos

## Descrição do Sistema
  O All4Class é uma plataforma pública para aproximar as comunidades escolares carentes e os órgãos públicos.
  Através do All4Class é possível criar requisições relatando vulnerabilidades e problemas que os alunos de uma escola nessa comunidade estão suscetíveis. Essas requisições são direcionadas à parte administrativa da plataforma, que aciona os órgãos solicitados para estrategicamente trabalhar nesses locais a fim de estancar a problemática no local. O All4Class auxilia no processo de criação de estratégia de atuação nessas comunidades por parte dos órgaos públicos através da geração de relatórios das requisições, mostrando através de gráficos e filtros quais escolas enfrentam maiores dificuldades e do que essa comunidade mais precisa. 


## Instalação

1. Instalar o gerenciador de dependencias [Composer](https://getcomposer.org/download/)

1. Realizar o clone do projeto
    
    **SSH**
    ```git
    git@gitlab.com:senac-tcc-2019/all4class/projetodesenvolvimento-michelluz.git
    ```

    **HTTPS**
    ```git
    https://gitlab.com/senac-tcc-2019/all4class/projetodesenvolvimento-michelluz.git
    ```

1. Abrir o projeto na sua IDE de preferência
2. Executar no terminal o comando `composer install --no-scripts`
3. Renomear o arquivo **.env.example** para **.env**
4. Executar no terminal o comando `php artisan key:generate`
5. Executar no terminal o comando `php artisan migrate`
6. Executar no terminal o comando `php artisan serve` 
7. Concluídos os passos, a plataforma deve estar rodando na porta relatada pelo bash da sua IDE.


## Heroku

Se você preferir acessar à plataforma diretamente online no Heorku, clique aqui -> [All4Class](http://all4class.herokuapp.com)

## Desenvolvido por
 * [Michel Luz](@mortalis)
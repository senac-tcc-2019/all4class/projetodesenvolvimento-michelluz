<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix'=>'admin', 'namespace'=>'Admin'], function() {
    Route::get('/', function() { return view('home'); });
    Route::post('escola/pesquisa', 'EscolaController@pesquisa')->name('escola.pesquisa');
    Route::post('instituicao/pesquisa', 'InstituicaoController@pesquisa')->name('instituicao.pesquisa');
    Route::post('responsavel/pesquisa', 'ResponsavelController@pesquisa')->name('responsavel.pesquisa');
    Route::get('escola/export', 'EscolaController@export')->name('escola.export');
    Route::resource('escola', 'EscolaController');
    Route::resource('bairro', 'BairroController');
    Route::resource('instituicao', 'InstituicaoController');
    Route::resource('responsavel', 'ResponsavelController');
    Route::resource('vulnerabilidade', 'VulnerabilidadeController');
    
    
    
});
Auth::routes();

Route::get('home', 'HomeController@index')->name('home');
Route::get('/', function() { return view('index');});
Route::get('mapa', function() { return view('mapa');})->name("mapa");
Route::get('participe', function() { return view('participe');})->name("participe");
Route::get('relatorio', function() { return view('relatorio');})->name('relatorio');
Route::post('enviar_requisicao', 'RequisicaoController@store')->name("enviar_requisicao");
Route::resource('admin/requisicao', 'RequisicaoController');
Route::get('resposta{id}', 'RequisicaoController@responder')->name('requisicao.resposta');
Route::post('resposta', 'RequisicaoController@enviaEmail')->name('resposta');
Route::post('requisicao/pesquisa', 'RequisicaoController@pesquisa')->name('requisicao.pesquisa');
Route::get('requisicao/export', 'RequisicaoController@export')->name('requisicao.export');








<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Responsavel extends Model
{
    protected $fillable = ['nome', 'telefone','email', 'endereco'];

    public $table = "responsavel";

    public function filtrar(Array $data){
        return $this->where(function($query) use ($data){
            
            if (isset($data['nome']))
                $query->where('nome', 'like', '%'.$data['nome'].'%');
                
            if (isset($data['telefone']))
                $query->where('telefone', 'like', '%'.$data['telefone'].'%');

            if (isset($data['email']))
                $query->where('email', 'like', '%'.$data['email'].'%');

            

        })
        ->paginate(10);
    }
}

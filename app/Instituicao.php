<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instituicao extends Model
{
    protected $fillable = ['nome', 'email', 'telefone', 'endereço', 'responsavel_id', 'vulnerabilidade_id'];

    public $table = "instituicao";


    public function responsavel(){
        return $this->belongsTo('App\Responsavel', 'responsavel_id');
    }
    public function vulnerabilidade(){
        return $this->belongsTo('App\Vulnerabilidade', 'vulnerabilidade_id');
    }

    public function filtrar(Array $data){
        return $this->where(function($query) use ($data){
            
            if (isset($data['nome']))
                $query->where('nome', 'like', '%'.$data['nome'].'%');
                
            if (isset($data['responsavel']))
                $query->where('responsavel_id', 'like', '%'.$data['responsavel'].'%');

        })
        ->paginate(10);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Escola extends Model
{
    protected $fillable = ['nome', 'bairro_id', 'endereco', 'telefone', 'responsavel_id'];

    public $table = "escola";
    public function bairro() {
        return $this->belongsTo('App\Bairro', 'bairro_id');
    }

    public function responsavel(){
        return $this->belongsTo('App\Responsavel', 'responsavel_id');
    }

    public function filtrar(Array $data){
        return $this->where(function($query) use ($data){
            
            //$dados = DB::select("select * from escola inner join bairro on escola.bairro_id = bairro.id where bairro.nome = '".$data['bairro']."'");
            
            if (isset($data['nome']))
                $query->where('nome', 'like', '%'.$data['nome'].'%');

            if (isset($data['bairro']))
                $query->where('bairro_id', 'like', '%'.$data['bairro'].'%');
                
            if (isset($data['responsavel']))
                $query->where('responsavel_id', 'like', '%'.$data['responsavel'].'%');

        })
        ->paginate(10);
    }

    // define os campos que serão editados na inclusão/alteração 
    // pelos métodos create e update
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Requisicao;
use App\Instituicao;
use App\Escola;
use App\Responsavel;
use App\Vulnerabilidade;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use PDF;


class RequisicaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vulnerabilidade = Vulnerabilidade::all();
        $requisicao = Requisicao::orderBy('id', 'desc')->paginate(10);
        return view('admin.caixa_list', ['requisicao'=>$requisicao], ['vulnerabilidade'=>$vulnerabilidade]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
        $this->validate($request, [
            'nome' => 'required|min:10',
            'email' => 'required',
            'telefone' => 'required|min:9|max:40',
            'escola_id' => 'required'
        ]);
        **/
        //return $request->all();
        Requisicao::create($request->all());
        return redirect()->route('mapa')->with('status', ' Requisição feita com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function responder($id)
    {
        $requisicao = Requisicao::find($id);
        
        return view("admin.resposta_requisicao", ["requisicao" => $requisicao]);
    }

    public function enviaEmail(Request $request)
    {
         
        $dados = $request->all();
        $requisicao = Requisicao::find($dados["id"]);
        
        $resposta = $request["mensagem"];
        Mail::send("Mail.resposta_requisicao", ["requisicao" => $requisicao,
                                              "mensagem" => $resposta], 
                                              function ($message)use ($requisicao) {
            $message->from("contato.revenda.herbie@gmail.com");
            $message->to($requisicao->emailCriador);
            $message->subject("Resposta do contato  #".$requisicao->id);
        });

        return redirect("admin/requisicao")
        ->with('status', 'Resposta enviada com sucesso!');;

    }

    public function pesquisa(Request $request, Requisicao $requisicao){
        $dados = $request->all();
        $vulnerabilidades = Vulnerabilidade::all();
        $filtro = $requisicao->filtrar($dados);
        /*$pdf = PDF::loadView('admin.rel_caixa', ['requisicao'=>$filtro]);
        return $pdf->download('Requisições.pdf');*/
        return view('admin.caixa_list', ['requisicao' => $filtro], ['vulnerabilidade' => $vulnerabilidades]);
    }

    public function export(Request $request, Requisicao $requisicao) {
        $dados = $request->all();
        $filtro = $requisicao->filtrar($dados);
        
        //$requisicao = Requisicao::all();
        // Send data to the view using loadView function of PDF facade
        $pdf = PDF::loadView('admin.rel_caixa', ['requisicao'=>$filtro]);
        return $pdf->download('Requisições.pdf');

    }
}

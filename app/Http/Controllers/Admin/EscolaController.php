<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Bairro;
use App\Escola;
use App\Responsavel;
use App\Filtro;
use App\Requisicao;
use Illuminate\Http\Request;
use PDF;

class EscolaController extends Controller
{
    public function index()
    {
        if (!Auth::check()) {
            return redirect("/home");       
        }
        $dados = Escola::paginate(10);
        $bairros = Bairro::all();
        $responsavel = Responsavel::all();
        $requisicao = Requisicao::all();
        
        
        return view('admin.escola_list', ['escola' => $dados],['bairro' => $bairros], ['responsavel' => $responsavel], ['requisicao' => $requisicao]);
    }

    public function create()
    {
        $responsavel = Responsavel::orderBy('nome')->get();
        $bairro = Bairro::orderBy('nome')->get();
        
        return view('admin.escola_form', 
                    ['responsavel'=>$responsavel, 'bairro'=>$bairro ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nome' => 'required',
            'endereco' => 'required',
                 
            
        ]);
        $dados = $request->all();
        $inc = Escola::create($dados);

        if ($inc) {
            return redirect()->route('escola.index')
                   ->with('status', $request->nome . ' inserida com sucesso!');     
        }

        
    }

    public function destroy($id)
    {
        $esc = Escola::find($id);
        $validacao = Requisicao::all();
        foreach($validacao as $val){
            if($val->escola_id == $esc->id){
                return redirect()->route('escola.index')
                            ->with('alert', 'Escolas vinculadas à requisições não podem ser removidas.'); 
            }
        }
        if ($esc->delete()) {
            return redirect()->route('escola.index')
                            ->with('status', $esc->nome . ' excluída com sucesso.');
        }
    }

    public function update(Request $request, $id)
    {
        
        $request->validate([
            'nome' => 'required',
            'endereco' => 'required',
                 
            
        ]);
        $esc = Escola::find($id);
        $esc->nome = $request->input('nome');
        $esc->responsavel_id = $request->input('responsavel_id');
        $esc->bairro_id = $request->input('bairro_id');
        $esc->endereco = $request->input('endereco');
        
        //$dados = $request->all();
        $inc = $esc->save();

        if ($inc) {
            return redirect()->route('escola.index')
                   ->with('status', $request->nome . ' alterada com sucesso!');     
        }



        /*// obtém os dados do form
        $dados = $request->all();

        // posiciona no registo a ser alterado
        $reg = Escola::find($id);

          // se o campo foto foi preenchido  e enviado (valido)
          
        // realiza a alteração
        $alt = $reg->update($dados);

        if ($alt) {
            return redirect()->route('escola.index')
                            ->with('status',  'Dados alterados!');
        }*/
        
    }

    public function show($id){
        return view('admin.escola_form', ['escola' => Escola::findOrFail($id)]);
    }
    

    public function edit($id)
    {
        
        // posiciona no registro a ser alterado e obtém seus dados
        $escola = Escola::find($id);

        $bairro = Bairro::orderBy('nome')->get();

        $responsavel = Responsavel::orderBy('nome')->get();
        
        return view('admin.escola_edit_form', compact('escola', 'bairro', 'responsavel'));
    }

    public function pesquisa(Request $request, Escola $escola){
        
        
        $dados = $request->all();
        $bairros = Bairro::all();

        $filtro = $escola->filtrar($dados);


        return view('admin.escola_list', ['escola' => $filtro], ['bairro' => $bairros]);
    }

    public function export() {
        /*$escola = Escola::all();

        return \PDF::loadView('admin.rel_escola', 
                            ['escola'=>$escola])->stream();*/

                            // Fetch all customers from database
        $escola = Escola::all();
        //$escola = $request->session()->get('cart');
        // Send data to the view using loadView function of PDF facade
        $pdf = PDF::loadView('admin.rel_escola', compact('escola'));
        return $pdf->download('Escolas.pdf');
        // If you want to store the generated pdf to the server then you can use the store function
        //$pdf->save(storage_path().'_filename.pdf');
        // Finally, you can download the file using download function

    }

    


    
}

<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Instituicao;
use App\Escola;
use App\Responsavel;
use App\Requisicao;
use Illuminate\Http\Request;

class InstituicaoController extends Controller
{
    public function index()
    {
        if (!Auth::check()) {
            return redirect("/home");       
        }
        $dados = Instituicao::paginate(5);
        
        return view('admin.instituicao_list', ['instituicao'=>$dados]);
    }

    public function create()
    {
        $responsavel = Responsavel::orderBy('nome')->get();
        
        return view('admin.instituicao_form',
                    ['responsavel'=>$responsavel]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nome' => 'required|unique:instituicao|min:2|max:60'

        ]);
        $dados = $request->all();
        $inc = Instituicao::create($dados);


        if ($inc) {
            return redirect()->route('instituicao.index')
                ->with('status', 'Instituição ' .$request->nome. ' inserida com sucesso!');
        }
    
    }

    public function destroy($id)
    {
        $esc = Instituicao::find($id);
        $validacao = Requisicao::all();
        foreach($validacao as $val){
            if($val->instituicao_id == $esc->id){
                return redirect()->route('instituicao.index')
                            ->with('alert', 'Instituições vinculadas à requisições não podem ser removidas.'); 
            }
        }
       
        if ($esc->delete()) {
            return redirect()->route('instituicao.index')
                            ->with('status', $esc->nome . ' excluída com sucesso!');
        }
    }

    public function pesquisa(Request $request, Instituicao $instituicao){
        $dados = $request->all();

        $filtro = $instituicao->filtrar($dados);

        return view('admin.instituicao_list', ['instituicao' => $filtro]);
    }

    public function edit($id)
    {
        
        // posiciona no registro a ser alterado e obtém seus dados
        $instituicao = Instituicao::find($id);

        $responsavel = Responsavel::orderBy('nome')->get();
        
        return view('admin.instituicao_edit_form', compact('instituicao', 'responsavel'));
    }

    public function update(Request $request, $id)
    {
        
        $request->validate([
            'nome' => 'required',
            
                 
            
        ]);
        $inst = Instituicao::find($id);
        $inst->nome = $request->input('nome');
        $inst->responsavel_id = $request->input('responsavel_id');
        $inst->endereco = $request->input('endereco');
        
        //$dados = $request->all();
        $inc = $inst->save();

        if ($inc) {
            return redirect()->route('instituicao.index')
                   ->with('status', $request->nome . ' alterada com sucesso!');     
        }



        /*// obtém os dados do form
        $dados = $request->all();

        // posiciona no registo a ser alterado
        $reg = Escola::find($id);

          // se o campo foto foi preenchido  e enviado (valido)
          
        // realiza a alteração
        $alt = $reg->update($dados);

        if ($alt) {
            return redirect()->route('escola.index')
                            ->with('status',  'Dados alterados!');
        }*/
        
    }
}

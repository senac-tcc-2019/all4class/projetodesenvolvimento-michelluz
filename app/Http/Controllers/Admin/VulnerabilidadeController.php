<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Vulnerabilidade;

use Illuminate\Http\Request;

class VulnerabilidadeController extends Controller
{
    public function index()
    {
        if (!Auth::check()) {
            return redirect("/home");       
        }
        $dados = Vulnerabilidade::paginate(5);
        
        return view('admin.vulnerabilidade_list', ['vulnerabilidade'=>$dados]);
    }

    public function create()
    {
        return view('admin.vulnerabilidade_form');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'titulo' => 'required|unique:vulnerabilidade|min:2|max:60'

        ]);
        $dados = $request->all();
        $inc = Vulnerabilidade::create($dados);


        if ($inc) {
            return redirect()->route('vulnerabilidade.index')
                ->with('status', 'Perfil de Vulnerabilidade ' .$request->nome. ' inserido com sucesso!');
        }
    }

    public function destroy($id)
    {
        $esc = Vulnerabilidade::find($id);
        $validacao = Requisicao::all();
        foreach($validacao as $val){
            if($val->vulnerabilidade_id == $esc->id){
                return redirect()->route('vulnerabilidade.index')
                            ->with('alert', 'Vulnerabilidades vinculadas à requisições não podem ser removidas.'); 
            }
        }
        if ($esc->delete()) {
            return redirect()->route('vulnerabilidade.index')
                            ->with('status', $esc->nome . ' excluído!');
        }
    }
}

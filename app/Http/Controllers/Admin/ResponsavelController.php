<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Bairro;
use App\Escola;
use App\Responsavel;
use App\Instituicao;
use Illuminate\Http\Request;

class ResponsavelController extends Controller
{
    public function index()
    {
        if (!Auth::check()) {
            return redirect("/home");       
        }
        //$dados = Responsavel::all();
        $dados = Responsavel::paginate(10);
        return view('admin.responsavel_list', ['responsavel' => $dados]);
    }

    public function create()
    {
        return view('admin.responsavel_form');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nome' => 'required',
            'email' => 'required',
            'telefone' => 'required', 
            
        ]);
        $dados = $request->all();
        $inc = Responsavel::create($dados);

        if ($inc) {
            return redirect()->route('responsavel.index')
                   ->with('status', $request->nome . ' inserido(a) com sucesso!');     
        }

        
    }

    public function destroy($id)
    {
        $esc = Responsavel::find($id);
        $instituicao = Instituicao::all();
        $escola = Escola::all();
        foreach($instituicao as $inst){
            if($inst->responsavel_id == $esc->id){
                return redirect()->route('responsavel.index')
                            ->with('alert', 'Pessoas vinculadas à instituições não podem ser removidas.'); 
            }
        }

        foreach($escola as $es){
            if($es->responsavel_id == $esc->id){
                return redirect()->route('responsavel.index')
                            ->with('alert', 'Pessoas vinculadas à escolas não podem ser removidas.'); 
            }
        }

        if ($esc->delete()) {
            return redirect()->route('responsavel.index')
                            ->with('status', $esc->nome . ' Excluído(a)!');
        }
    }

    public function update(Request $request, $id)
    {
        
        $request->validate([
            'nome' => 'required',
            'email' => 'email',
                 
            
        ]);
        $res = Responsavel::find($id);
        $res->nome = $request->input('nome');
        $res->email = $request->input('email');
        $res->endereco = $request->input('endereco');
        $res->telefone = $request->input('telefone');
        
        //$dados = $request->all();
        $inc = $res->save();

        if ($inc) {
            return redirect()->route('responsavel.index')
                   ->with('status', $request->nome . ' alterada com sucesso!');     
        }


        
    }

    public function edit($id)
    {
        
        // posiciona no registro a ser alterado e obtém seus dados
        $responsavel = Responsavel::find($id);

        
        return view('admin.responsavel_edit_form', compact('responsavel'));
    }

    public function pesquisa(Request $request, Responsavel $responsavel){
        $dados = $request->all();

        $filtro = $responsavel->filtrar($dados);

        return view('admin.responsavel_list', ['responsavel' => $filtro]);
    }
}

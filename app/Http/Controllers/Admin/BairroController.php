<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Bairro;
use App\Escola;
use App\Responsavel;
use Illuminate\Http\Request;

class BairroController extends Controller
{
    public function index()
    {
        if (!Auth::check()) {
            return redirect("/home");       
        }
        $dados = Bairro::paginate(5);
        
        return view('admin.bairro_list', ['bairro'=>$dados]);
    }

    public function create()
    {
        return view('admin.bairro_form');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nome' => 'required|unique:bairro|min:2|max:60'

        ]);
        $dados = $request->all();
        $inc = Bairro::create($dados);


        if ($inc) {
            return redirect()->route('bairro.index')
                ->with('status', 'Bairro ' .$request->nome. ' inserido com sucesso!');
        }
    }
    
    public function destroy($id)
    {
        $esc = Bairro::find($id);

        $validacao = Escola::all();
        foreach($validacao as $val){
            if($val->bairro_id == $esc->id){
                return redirect()->route('bairro.index')
                            ->with('alert', 'Bairros vinculados à escolas não podem ser removidos.'); 
            }
        }
       
        if ($esc->delete()) {
            return redirect()->route('bairro.index')
                            ->with('status', $esc->nome . ' excluído!');
        }
    }

    public function update(Request $request, $id)
    {

        $request->validate([
            'nome' => 'required',      
            
        ]);

        $bairro = Bairro::find($id);
        $bairro->nome = $request->input('nome');
        $inc = $bairro->save();


        if ($inc) {
            return redirect()->route('bairro.index')
                            ->with('status', 'Dados alterados!');
        }
        
    }

    public function edit($id)
    {
        
        // posiciona no registro a ser alterado e obtém seus dados
        $bairro = Bairro::find($id);

        
        
        return view('admin.bairro_edit_form', compact('bairro'));
    }
}

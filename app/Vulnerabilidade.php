<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vulnerabilidade extends Model
{
    protected $fillable = ['titulo', 'descricao'];

    public $table = "vulnerabilidade";
}

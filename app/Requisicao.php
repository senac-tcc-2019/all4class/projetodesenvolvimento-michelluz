<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Requisicao extends Model
{
    protected $table = 'requisicao';
    protected $fillable = ['descricao', 'nomeCriador', 'emailCriador', 'telefoneCriador', 'instituicao_id', 'escola_id',
    'vulnerabilidade_id','isLida'];

    public function instituicao(){
        return $this->belongsTo('App\Instituicao', 'instituicao_id');

    }

    public function vulnerabilidade(){
        return $this->belongsTo('App\Vulnerabilidade', 'vulnerabilidade_id');
    }

    public function escola(){
        return $this->belongsTo('App\Escola', 'escola_id');
    }

    public function filtrar(Array $data){
        return $this->where(function($query) use ($data){
            
            if (isset($data['escola']))
                $query->where('escola_id', 'like', '%'.$data['escola'].'%');

            if (isset($data['vulnerabilidade']))
                $query->where('vulnerabilidade_id', 'like', '%'.$data['vulnerabilidade'].'%');
                
            if (isset($data['email']))
                $query->where('emailCriador', 'like', '%'.$data['email'].'%');

            

            if (isset($data['data'])) {
                $query->where('created_at', 'like', '%'.$data['data'].'%');
                
            }
        })
        ->paginate(10);
    }
}
